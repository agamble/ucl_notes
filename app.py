from flask import Flask, session, redirect, url_for, escape, request, render_template, abort

import os

app = Flask(__name__)


this_path = os.path.dirname(os.path.abspath(__file__))

@app.route('/')
def index():
	context = {}
	module_list = os.listdir(this_path + '/templates/modules')

	return render_template('index.html', context=context )

# @app.route('/architecture/')
# @app.route('/architecture/<id>')
# def architecture(id=None):

# 	if id is None:
# 		return "hello"
# 	else:
# 		# user has requested a specific note
# 		return render_template('architecture/' + id + '.html')


@app.route('/<module>/<id>')
@app.route('/<module>')
def module(module=None, id=None):
	module_list = os.listdir(this_path + '/templates/modules/')
	if module in module_list:
		if id is None:
			# user requested the list of notes
			notes_list = os.listdir(this_path + '/templates/modules/' + module + '/')
			titles_array = []

			# get the title that I used for that lecture so that we can list them
			for note in notes_list:
				note_path = this_path + '/templates/modules/' + module + '/' + note
				file = open(note_path, 'r')
				for line in file:
					# go through each line and discover if I used the <h1> tag
					# this function returns -1 if it is not found, 
					# and something else (the character number) if it is
					if line.find('<h1>') is not -1:
						title = line.split('<h1>')[1].split('</h1>')[0]
						titles_array.append(title)
						break

			return render_template('modules/module_index.html', titles_array=titles_array, title=module, module=module, id=None)

		filename = "modules/" + module + "/" + id + ".html"
		return render_template(filename, module=module, id=id, title=module)


	else:
		abort(404)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5050)